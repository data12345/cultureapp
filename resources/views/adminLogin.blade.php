@extends('layouts.login')
@section('content') 
<div class="row justify-content-center" style="margin-top:100px;margin-bottem:auto;">
<div class="card col-lg-4 pb-3">
    <div class="card-header card-header-primary mb-6 text-center">
     <h1>Admin Login</h1>
    </div>
    <div class="card_body">
        <form class="form" method="post" action="{{ route('login') }}" enctype="multipart/form-data">
            @csrf
            </br>
            <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" name="email"  value="{{old('email')}}" id="email">
                  @error('email')
                                    <span class="" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" name ="password" id="pwd">
                @error('password')
                    <span class="" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
               
            </div>
            <span class="ialert" role="alert">
                    <strong>hjkshjksfhdjk</strong>
                    </span>
             @if(Session::has('error'))
                    <span class="" role="alert">
                    <strong>{{ Session::get("error") }}</strong>
                    </span>
                @endif
            <div class="text-center">
            <button type="submit" class="btn btn-primary">Log in</button>
           </div>
        </form>
    </div>
</div>
</div>
@endsection