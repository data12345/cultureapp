@extends('layouts.admin')

@section('content')
    <h1 class="page-heading">Category List</h1>
 <div class="main-panel cat-list">
     <div class="col-md-12">
              @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
              @if(session()->has('error'))
              <div class="alert alert-danger">
                {{ session()->get('error') }}
              </div>
              @endif
              <div class="card">
                {{--<div class="card-header card-header-primary">
                  <h4 class="card-title ">Category List</h4>
                <!--  <p class="pull-right card-category"><a href="{{ Route('CategoryCreate') }}" >Add Category</a></p> -->
                </div>--}}
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" >
                      <thead class=" text-primary">
                        <tr>
                        <th>S.no</th>
                        <th>Title</th>

                        <th> Image</th>
                        <th>Action</th>
                      </tr></thead>
                      <tbody>
                      @foreach($list as $val)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{ $val->title}}</td>
                          <td>
                          <img src="{{ $val->image}}" width="10%">
                          </td>
                          <td> <!-- <a href="#" data-url="{{ Route('ActivityDestroy',$val->id) }}" class="deleteData" onClick="deleteFunction();">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                          </a>-->
                          <a href="{{ Route('CategoryEdit',$val->id) }}" >
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                          </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
    </div>
@endsection
