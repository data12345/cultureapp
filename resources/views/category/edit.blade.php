@extends('layouts.admin')
@section('content')

<div class="main-panel edit-cat">
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Category</h4>
                </div>
                <div class="card-body">
                  <form method="post" action="{{ Route('CategoryUpdate',$data->id) }}" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Title</label>
                          <input type="text" value="{{ $data->title }}" name="title" class="form-control" >
                          <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                      </div>
                    </div>
                       <div class="row">
                      <div class="col-md-6">
                       <div class="col-md-6">
                        <img src="{{$data->image}}" width="50%">
                      </div>
                        <div class="">
                          <label class="bmd-label-floating">Image</label>
                           <input type="file" accept="image/*" name="image" class="">
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        </div>
                      </div>
                           <div class="clearfix"></div>
                       </div>
                    <button type="submit" class="btn btn-primary pull-right">Save</button>

                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
@endsection
