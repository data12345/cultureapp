@extends('layouts.admin')

@section('content')
 <div class="main-panel">
      <!-- Navbar -->
      <!-- End Navbar -->
     <h1 class="page-heading">Dashboard</h1>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <a href="{{route('EventList')}}">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">event</i>
                  </div>
                  <p class="card-category">Events</p>
                  <h3 class="card-title">{{$event}}

                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons text-danger">warning</i>

                  </div>
                </div>
              </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <a href="{{route('ActivityList')}}">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">content_paste</i>
                  </div>
                  <p class="card-category">Activities</p>
                  <h3 class="card-title">{{ $activity }}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> activities
                  </div>
                </div>
              </div>
                </a>
            </div>
{{--            <div class="col-lg-4 col-md-6 col-sm-6">--}}
{{--              <div class="card card-stats">--}}
{{--                <div class="card-header card-header-danger card-header-icon">--}}
{{--                  <div class="card-icon">--}}
{{--                    <i class="material-icons">done</i>--}}
{{--                  </div>--}}
{{--                  <p class="card-category">Destinations</p>--}}
{{--                  <h3 class="card-title">{{$event}}</h3>--}}
{{--                </div>--}}
{{--                <div class="card-footer">--}}
{{--                  <div class="stats">--}}
{{--                    <i class="material-icons">local_offer</i> Destinations--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--              </div>--}}
{{--            </div>--}}
            <!-- div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-twitter"></i>
                  </div>
                  <p class="card-category">Followers</p>
                  <h3 class="card-title">+245</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                  </div>
                </div>
              </div>
            </div> -->
          </div>


        </div>
      </div>

    </div>
@endsection
