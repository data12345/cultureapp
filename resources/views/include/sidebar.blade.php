<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">

        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav">

                <li class="nav-item dropdown">
                    <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">person</i>
                        <p class="d-lg-none d-md-block">
                            Account
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                        <!--  <a class="dropdown-item" href="#">Profile</a> -->

                        <a class="dropdown-item" href="{{ Route('AdminCreate') }}">Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="sidebar" data-color="purple" data-background-color="white"
     data-image="{{ asset('assets/img/sidebar-1.jpg') }}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="{{ Route('home') }}" class="simple-text logo-normal">
            Culture App
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item {{ (Request::segment(1) == "home") ? "active" : ""}} ">
                <a class="nav-link" href="{{ Route('home') }}">
                    <i class="material-icons">show_chart</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(1) == "category") ? "active" : ""}}">
                <a class="nav-link" href="{{ Route('CategoryList') }}">
                    <i class="material-icons">layers</i>
                    <p>Categories</p>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(1) == "Eventcategory") ? "active" : ""}}">
                <a class="nav-link" href="{{ Route('EventCategoryList') }}">
                    <i class="material-icons">event_note</i>
                    <p>Event Categories</p>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(1) == "event") ? "active" : ""}}">
                <a class="nav-link" href="{{ Route('EventList') }}">
                    <i class="material-icons">event</i>
                    <p>Events</p>
                </a>
            </li>
            <li class="nav-item  {{ (Request::segment(1) == "activity") ? "active" : ""}}">
                <a class="nav-link" href="{{ Route('ActivityList') }}">
                    <i class="material-icons">content_paste</i>
                    <p>Activities</p>
                </a>
            </li>
            <li class="nav-item {{ (Request::segment(1) == "admin") ? "active" : ""}}">
                <a class="nav-link" href="{{ Route('AdminCreate') }}">
                    <i class="material-icons">build</i>
                    <p>Settings</p>
                </a>
            </li>

        </ul>
    </div>
