
@extends('layouts.admin')
@section('content')

<div class="main-panel">
<!-- <div>
 <h3>Profle Settings </h3>
 </div -->
 @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
              @if(session()->has('error'))
              <div class="alert alert-danger">
                {{ session()->get('error') }}
              </div>
              @endif
      <div class="row mt-2 justify-content-center setting-page">

    <div class="card col-sm-5">
    <div class="card-header card-header-primary text-center">
     <h3>Edit Profile</h3>
    </div>

    <div class="card_body">
        <form class="form" method="post" action="{{ Route('AdminUpdate',Auth::User()->id) }}" enctype="multipart/form-data">
            @csrf
            </br>
             <div class="form-group">
                <label for="first_name">Name:</label>
                <input type="text" class="form-control" name="name"  value="{{ Auth::User()->name }}" id="first_name">
               <span class="text-danger">{{ $errors->first('name') }}</span>
             </div>
            <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" name="email"  value="{{ Auth::User()->email }}" disabled="" id="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="text-center">
            <button type="submit" class="btn btn-info">Submit</button>
           </div>
        </form>
    </div>
    </div>
    <div class="card col-sm-5 ml-2">
        <div class="card-header card-header-primary  text-center">
            <h3>Change Password</h3>
        </div>
        <div class="card_body">
            <form class="form"  action="{{ Route('AdminChangePassword',Auth::User()->id) }}" >
                @csrf
                </br>
                    <div class="form-group">
                    <label for="old_password">Old Password:</label>
                    <input type="password" class="form-control" name="old_password"  placeholder="*******" id="old_password">
                     <span class="text-danger">{{ $errors->first('old_password') }}</span>
                    </div>
                    <div class="form-group">
                    <label for="new_password">New Password:</label>
                    <input type="password" class="form-control" name="new_password"  placeholder="*******" id="new_password">
                     <span class="text-danger">{{ $errors->first('new_password') }}</span>
                    </div>
                <div class="form-group">
                    <label for="confirm_password">Confirm Password:</label>
                    <input type="password" class="form-control" name="confirm_password"  placeholder="*******" id="confirm_password">
                     <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                     @if(isset($error))
                      <span class="text-danger">{{ $error }}</span>
                      @endif
                </div>
                <div class="text-center">
                <button type="submit" class="btn btn-info">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

    </div>
@endsection
