
@extends('layouts.admin')
@section('content')

<div class="main-panel edit-cat">
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
             @if(session()->has('message'))
              <div class="alert alert-success">
                {{ session()->get('message') }}
              </div>
              @endif
              @if(session()->has('error'))
              <div class="alert alert-danger">
                {{ session()->get('error') }}
              </div>
              @endif
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add activity</h4>
                </div>
                <div class="card-body">
                  <form method="post" action="{{ Route('ActivityStore') }}" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Title</label>
                          <input type="text" value="{{ old('title') }}" name="title" class="form-control" >
                          <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                      </div>

                    </div>
                       <div class="row">
                      <div class="col-md-6">
                        <div class="">
                          <label class="bmd-label-floating">Image</label>
                           <input type="file" accept="image/*" name="image" class="">
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        </div>
                      </div>
                      <div class="col-md-3 mt-4">
                        <div class="form-group">
                          <label class="bmd-label-floating ">End Date</label>
                          <input type="date" value="{{ old('end_date') }}" name="end_date" class="form-control datetimepicker1">
                           <span class="text-danger">{{ $errors->first('end_date') }}</span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating ">Time</label>
                          <input type="time" value="{{ old('time') }}" name="time" class="form-control datetimepicker2">
                           <span class="text-danger">{{ $errors->first('time') }}</span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Register Link</label>
                          <input type="text" value="{{ old('register_link') }}" name="register_link" class="form-control">
                           <span class="text-danger">{{ $errors->first('register_link') }}</span>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Description</label>
                          <textarea id="editor4" name="description" class="form-group form-control">{{ old('description') }}</textarea>
                           <span class="text-danger">{{ $errors->first('description') }}</span>
                        </div>
                      </div>

                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
@endsection
