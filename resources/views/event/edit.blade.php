
@extends('layouts.admin')
@section('content')

<div class="main-panel edit-cat">
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Event</h4>
                </div>
                <div class="card-body">
                  <form method="post" action="{{ Route('EventUpdate',$data->id) }}" enctype="multipart/form-data">
                  @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Title</label>
                          <input type="text" value="{{ $data->title }}" name="title" class="form-control" >
                          <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                      </div>
                    </div>
                       <div class="row">
                      <div class="col-md-6">
                       <div class="col-md-6">
                        <img src="{{$data->image}}" width="30%">
                      </div>
                        <div class="mb-3">
                          <label class="bmd-label-floating">Image</label>
                           <input type="file" accept="image/*" name="image" class="">
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">End Date</label>
                          <input type="date" value="{{ date('Y-m-d', strtotime($data->end_date))}}" name="end_date" class="form-control datetimepicker1">
                           <span class="text-danger">{{ $errors->first('end_date') }}</span>
                        </div>
                      </div>
                       <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Time</label>
                          <input type="text" value="{{ $data->time}}" name="time" class="form-control datetimepicker2">
                           <span class="text-danger">{{ $errors->first('time') }}</span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Category</label>
                                <select class="form-control" name="category">
                                    <option value=" "> Select Category</option>
                                    @foreach($category as $val)
                                        <option value="{{$val->id}}" {{($val->id==$data->category_id) ? "selected" : " "}}>{{$val->title}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger">{{ $errors->first('category') }}</span>
                            </div>
                        </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Register Link</label>
                          <input type="text" value="{{ $data->register_link }}" name="register_link" class="form-control">
                           <span class="text-danger">{{ $errors->first('register_link') }}</span>
                        </div>
                      </div>
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Website Link</label>
                          <input type="text" value="{{ $data->website_link }}" name="website_link" class="form-control">
                           <span class="text-danger">{{ $errors->first('website_link') }}</span>
                        </div>
                      </div>
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Free Admission Link</label>
                          <input type="text" value="{{ $data->free_admission }}" name="free_admission" class="form-control">
                           <span class="text-danger">{{ $errors->first('free_admission') }}</span>
                        </div>
                      </div>
                        <input type="hidden" name="latLong" class="latLong" value="">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Address</label>
                                <input id="autocomplete"
                                       onFocus="geolocate()" type="text" value="{{ $data->address }}" name="address" class="form-control">
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                            </div>
                        </div>

                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Description</label>
                          <textarea name="description" id="editor2" class="form-group form-control">{{ $data->description }}</textarea>
                           <span class="text-danger">{{ $errors->first('description') }}</span>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Save</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
@endsection
