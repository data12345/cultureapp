@extends('layouts.admin')
@section('content')

    <div class="main-panel">
        <h1 class="page-heading">Event List</h1>
        <p class="pull-right card-category"><a href="{{ Route('EventCreate') }}">Add Event</a></p>
        <div class="col-md-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
            <div class="card">
                {{--<div class="card-header card-header-primary">
                    <h4 class="card-title ">Event List</h4>

                </div>--}}
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                            <tr>
                                <th>S.no</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>End Date</th>
                                <th> Register Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list as $val)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $val->title}}</td>
                                    <td>{{ $val['category']->title }}</td>
                                    <td><a href="https://www.google.com/maps/place/{{$val->address}}" target="_blank">{{ \Illuminate\Support\Str::limit($val->address, $limit = 30, $end = '...') }}</a> </td>

                                    <td>{{ date('d-M-Y', strtotime($val->end_date))}} - {{$val->time}}</td>
                                    <td class="text-primary">
                                        <a target="_blank" href="{{ $val->register_link }}">Register Link </a>
                                    </td>
                                    <td><a href="#" data-url="{{ Route('EventDestroy',$val->id) }}"
                                           class="deleteEventFunction">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                        <a href="{{ Route('EventEdit',$val->id) }}">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
