<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {

    return view('auth.login');
});*/

/*Route::get('/test', function () {

    return view('adminLogin');
});*/

Route::get('/', 'HomeController@index')->name('home');

//Forgot PasswordRoute
Route::any('/sendForgotPasswordOtp', 'Admin\ForgotPasswordController@sendForgotPasswordOtp')->name('sendForgotPasswordOtp');
Route::any('/ForgotPassword/{email}', 'Admin\ForgotPasswordController@ForgotPassword')->name('ForgotPassword');
Route::any('/changePassword', 'Admin\ForgotPasswordController@changePassword')->name('changePassword');

Auth::routes();


Route::group(['middleware'=>'auth'], function(){
Route::get('/home', 'HomeController@index')->name('home');


// Admin Route
Route::group(['prefix'=>'admin'], function(){
	Route::get('/create', 'Admin\AdminController@create')->name('AdminCreate');
	Route::any('/update/{id}', 'Admin\AdminController@update')->name('AdminUpdate');
	Route::any('/changePassword/{id}', 'Admin\AdminController@changePassword')->name('AdminChangePassword');
});

// category Route
Route::group(['prefix'=>'category'], function(){
	Route::get('/list', 'Admin\CategoryController@index')->name('CategoryList');
	Route::get('/create', 'Admin\CategoryController@create')->name('CategoryCreate');
	Route::any('/store', 'Admin\CategoryController@store')->name('CategoryStore');
	Route::any('/edit/{id}', 'Admin\CategoryController@edit')->name('CategoryEdit');
	Route::any('/delete/{id}', 'Admin\ActivityController@destroy')->name('ActivityDestroy');
	Route::any('/update/{id}', 'Admin\CategoryController@update')->name('CategoryUpdate');
});


// event category Route
    Route::group(['prefix'=>'Eventcategory'], function(){
        Route::get('/list', 'Admin\CategoryController@EventCategory')->name('EventCategoryList');
        Route::get('/create', 'Admin\CategoryController@createEventCategory')->name('EventCategoryCreate');
        Route::any('/store', 'Admin\CategoryController@storeEventCategory')->name('EventCategoryStore');
        Route::any('/edit/{id}', 'Admin\CategoryController@Eventedit')->name('EventCategoryEdit');
        Route::any('/delete/{id}', 'Admin\CategoryController@destroy')->name('EventCategoryDestroy');
        Route::any('/update/{id}', 'Admin\CategoryController@Eventupdate')->name('EventCategoryUpdate');
    });

// activity Route
Route::group(['prefix'=>'activity'], function(){
	Route::get('/list', 'Admin\ActivityController@index')->name('ActivityList');
	Route::get('/create', 'Admin\ActivityController@create')->name('ActivityCreate');
	Route::any('/store', 'Admin\ActivityController@store')->name('ActivityStore');
	Route::any('/edit/{id}', 'Admin\ActivityController@edit')->name('ActivityEdit');
	Route::any('/delete/{id}', 'Admin\ActivityController@destroy')->name('ActivityDestroy');
	Route::any('/update/{id}', 'Admin\ActivityController@update')->name('ActivityUpdate');
});


// Event Route
Route::group(['prefix'=>'event'], function(){
	Route::get('/list', 'Admin\EventController@index')->name('EventList');
	Route::get('/create', 'Admin\EventController@create')->name('EventCreate');
	Route::any('/store', 'Admin\EventController@store')->name('EventStore');
	Route::any('/edit/{id}', 'Admin\EventController@edit')->name('EventEdit');
	Route::any('/delete/{id}', 'Admin\EventController@destroy')->name('EventDestroy');
	Route::any('/update/{id}', 'Admin\EventController@update')->name('EventUpdate');
});


});
