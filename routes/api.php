<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//category Route
Route::group(['prefix'=>'category'], function(){
	Route::any('/list', 'Api\CategoryController@index');
	Route::any('/view/{id}', 'Api\ActivityController@show');
});

//event category Route
Route::group(['prefix'=>'EventCategory'], function(){
    Route::any('/list', 'Api\CategoryController@eventCategoryList');
});

//Destinations  category Route
Route::group(['prefix'=>'Destination'], function(){
    Route::any('/list', 'Api\EventController@Destinations');
});

//Activity Route
Route::group(['prefix'=>'activity'], function(){
	Route::any('/list', 'Api\ActivityController@index');
	Route::any('/view/{id}', 'Api\ActivityController@show');
});

// Event Route
Route::group(['prefix'=>'event'], function(){
	Route::any('/list', 'Api\EventController@index');
	Route::any('/view/{id}', 'Api\EventController@show');
});

//Search

Route::any('/search', 'Api\ActivityController@search');


