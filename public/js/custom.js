$(".deleteEventFunction" ).on( "click", function() {
  var url=$(this).attr('data-url');
  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    Swal.fire(
      'Deleted!',
      'Your Event has been deleted.',
      'success'
    )
    setTimeout(function(){
      window.location.href=url;
     }, 3000)

  }
})
});

$(".deleteActivityFunction" ).on( "click", function() {
  var url=$(this).attr('data-url');
  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    Swal.fire(
      'Deleted!',
      'Your activity has been deleted.',
      'success'
    )
    setTimeout(function(){
      window.location.href=url;
     }, 3000)

  }
})
});

$(".deleteEventCategory" ).on( "click", function() {
    var url=$(this).attr('data-url');
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            setTimeout(function(){
                window.location.href=url;
            }, 3000)

        }
    })
});

$( document ).ready(function() {
$(function () {
    $('.datetimepicker1').datetimepicker({
      format:"Y-m-d",
      timepicker:false
    });

    $('.datetimepicker2').datetimepicker({
      format:"H:i",
      datepicker:false
    });
    $('#datetimepicker3').datetimepicker({
      format:"H:i:s",
      datepicker:false
    });

    });

CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;

};
CKEDITOR.replace('editor1');
CKEDITOR.replace('editor2');
CKEDITOR.replace('editor3');
CKEDITOR.replace('editor4');

});

//Google address

var placeSearch, autocomplete;

var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name',
  //'administrative_area_level_3':'sholt_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), {types: ['geocode']});

  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(['address_component']);
    autocomplete.setFields(['geometry']);

  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
    autocomplete.addListener('geometry', fillInAddress);


}

function fillInAddress() {
  //infowindow.close();
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
  console.log(place);

  var latLog=place.geometry.location;
    $('.latLong').val(latLog);

// Then do whatever you want with them


  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];

    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }


}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
        autocomplete.setBounds(circle.getBounds());

    });
  }
}

$(document).ready(function () {
    setTimeout(function(){ $('.table.dataTable').parent('.col-sm-12').addClass('table-wrapper') }, 2500);

});

