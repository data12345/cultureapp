<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function getImageAttribute($value)
    {
        return asset('/eventImages/'.$value);
    }

    public function Category()
    {
        return $this->belongsTo('App\EventCategory');
    }

    public function getEndDateAttribute($value)
    {
        return  date('Y-m-d', strtotime($value));
    }

    public function getLatitudeAttribute($value)
    {
        return  (float)$value;
    }

    public function getLongitudeAttribute($value)
    {
        return  (float)$value;
    }


}
