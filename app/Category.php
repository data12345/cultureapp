<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{ 
	public function getImageAttribute($value)
    {
        return asset('/CategoryImages/'.$value);
    }
}
