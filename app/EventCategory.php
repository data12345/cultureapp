<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventCategory extends Model
{
    use SoftDeletes;
    public function getImageAttribute($value)
    {
        return asset('/CategoryImages/'.$value);
    }


}
