<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{

	use SoftDeletes;

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }


    public function getImageAttribute($value)
    {
        return asset('/activityImages/'.$value);
    }

    public function getEndDateAttribute($value)
    {
        return  date('Y-m-d', strtotime($value));
    }
}
