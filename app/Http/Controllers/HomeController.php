<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Activity;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['event'] = Event::count();
        $data['activity'] = Activity::count();
        return view('home', $data);
    }


    public function loginView()
    {

        if (Auth::check()) {
            return redirect('home')->route();
        } else {
            return view('auth.login');
        }
    }
}
