<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Activity;
use App\Event;
use Session;
use Validator;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->language == "en") {
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        } elseif ($request->language == "ar") {
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
        $data['status'] = 1;
        $data['data'] = Activity::Active()->get();
        if (count($data['data']) > 0) {
            return $data;
        } else {
            $error['status'] = 0;
            return $error;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $data['data'] = [];
        if ($request->language == "en") {
            $validator = Validator::make($request->all(), [
                'keyword' => 'required',
                'type' => 'required',
            ]);

            if ($validator->fails()) {
                $response['status'] = 0;
                $response['message'] = $validator->errors()->first();
                return $response;
            }
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        } elseif ($request->language == "ar") {
            if (empty($request->keyword)) {
                $response['status'] = 0;
                $response['message'] = "حقل الكلمة الأساسية مطلوب.";
                return $response;
            }
            if (empty($request->type)) {
                $response['status'] = 0;
                $response['message'] = "حقل الكتابة مطلوب.";
                return $response;
            }
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
        if ($request->type == "Activity") {
            $data['data'] = Activity::where('title', 'LIKE', "%{$request->keyword}%")
                //->orWhere('email', 'LIKE', "%{$searchTerm}%")
                ->get();
        } elseif ($request->type == "Event") {
            $data['data'] = Event::where('title', 'LIKE', "%{$request->keyword}%")
                //->orWhere('email', 'LIKE', "%{$searchTerm}%")
                ->get();
        }


        if (count($data['data']) > 0) {
            return $data;
        } else {
            $error['status'] = 0;
            return $error;
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data['status'] = 1;
        if ($request->language == "en") {
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        } elseif ($request->language == "ar") {
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
        $data['data'] = Activity::Active()->find($id);

        if ($data['data']) {
            return $data;
        } else {
            $error['status'] = 0;
            return $error;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
