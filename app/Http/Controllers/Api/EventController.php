<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use Session;
use Validator;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['status'] = 1;
        if ($request->language=="en"){
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        }elseif ($request->language=="ar"){
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
        $data['data'] = Event::Active()->get();
        if (count($data['data']) > 0) {

            return $data;
        } else {
            $error['status'] = 0;
            return $error;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Destinations(Request $request)
    {
        $data['status'] = 1;
        if ($request->language=="en"){
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        }elseif ($request->language=="ar"){
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
       
        $events = Event::whereHas('Category', function ($query) use ($request) {
            if ($request->keyword){
                $query->where('title', 'LIKE', "%{$request->keyword}%");
            }
        })->where(function ($query) use ($request) {
            if ($request->category_id){
                $query->where('category_id', '=',$request->category_id);
            }
            if ($request->keyword){
                //$query->where('title', 'LIKE', "%{$request->keyword}%");
            }
        })->with('Category')->get();
        $catehoryIds = [];

        foreach ($events as $val=> $event) {

            if (in_array($event->category_id, $catehoryIds))
            {
               //    echo "Match found";
            }
            else
            {
                $catehoryIds[] = (int)$event->category_id;
            }
        }


        $data['data']['events'] = $events;
        $data['data']['category_ids']=  $catehoryIds;
        if (count($data['data']['events']) > 0) {
            return $data;
        } else {
            $error['status'] = 0;
            return $error;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $data['status'] = 1;
        if ($request->language=="en"){
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        }elseif ($request->language=="ar"){
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
        $data['data'] = Event::Active()->find($id);

        if ($data['data']) {
            return $data;
        } else {
            $error['status'] = 0;
            return $error;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
