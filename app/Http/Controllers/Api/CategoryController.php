<?php

namespace App\Http\Controllers\Api;

use App\EventCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Session;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['status'] = 1;
        if ($request->language=="en"){
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        }elseif ($request->language=="ar"){
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
        $data['shareLink']="https://ksystems.invisionapp.com/public/share/4EXJN35KR#/screens/475468450  ";
        $data['data']=Category::get();
        if (count($data['data']) > 0) {

            return $data;
        }else{
            $error['status']=0;
            $error['message']="Data not Found";
            return $error;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function eventCategoryList(Request $request)
    {
        $data['status'] = 1;
        if ($request->language=="en"){
            $data['message'] = "Data Found";
            $error['message'] = "Data not Found";
        }elseif ($request->language=="ar"){
            $data['message'] = "العثور على البيانات";
            $error['message'] = "لم يتم العثور على بيانات";
        }
        $data['data']=EventCategory::get();
        if (count($data['data']) > 0) {

            return $data;
        }else{
            $error['status']=0;
            return $error;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
