<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\EventCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Session;
use Validator;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list']=Category::get();
        return view('category.list',$data);
    }

    public function EventCategory()
    {
        $data['list']=EventCategory::get();
        return view('eventCategory.list',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }


    public function createEventCategory()
    {
        return view('eventCategory.create');

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
        'title'    => 'required',
        'image'     => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $category = new Category;
        if($request->hasFile('image')){
        //get image file.
        $image = $request->image;
        //get just extension.
        $ext = $image->getClientOriginalExtension();
        //make a unique name
        $filename = uniqid().'.'.$ext;

        request()->image->move(public_path('CategoryImages'), $filename);
        $category->image = $filename;

        }
        $category->title = $request->title;

        if ($category->save()) {
        return redirect()->route('CategoryList')->with('message', 'Category Added Successfuly ');
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }

    }

    public function storeEventCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'    => 'required',
            'image'     => 'required',
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $category = new EventCategory();
        if($request->hasFile('image')){
            //get image file.
            $image = $request->image;
            //get just extension.
            $ext = $image->getClientOriginalExtension();
            //make a unique name
            $filename = uniqid().'.'.$ext;

            request()->image->move(public_path('CategoryImages'), $filename);
            $category->image = $filename;

        }
        $category->title = $request->title;

        if ($category->save()) {
            return redirect()->route('EventCategoryList')->with('message', 'Category Added Successfuly ');
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data']=Category::find($id);
        return view('category.edit',$data);
    }

    public function EventEdit($id)
    {
        $data['data']=EventCategory::find($id);
        return view('eventCategory.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator = Validator::make($request->all(), [
        'title'    => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $category = Category::find($id);

        if($request->hasFile('image')){
        //get image file.
        $image = $request->image;
        //get just extension.
        $ext = $image->getClientOriginalExtension();
        //make a unique name
        $filename = uniqid().'.'.$ext;

        request()->image->move(public_path('CategoryImages'), $filename);
        $category->image = $filename;

        }
        $category->title = $request->title;

        if ($category->save()) {
        return redirect()->route('CategoryList')->with('message', 'Category Update Successfuly ');
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }


    }


    public function Eventupdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'    => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $category = EventCategory::find($id);

        if($request->hasFile('image')){
            //get image file.
            $image = $request->image;
            //get just extension.
            $ext = $image->getClientOriginalExtension();
            //make a unique name
            $filename = uniqid().'.'.$ext;

            request()->image->move(public_path('CategoryImages'), $filename);
            $category->image = $filename;

        }
        $category->title = $request->title;

        if ($category->save()) {
            return redirect()->route('EventCategoryList')->with('message', 'Category Update Successfuly ');
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $EventCategory= EventCategory::find($id);
        $EventValiation= Event::where('category_id',$id)->first();
        if ($EventValiation){
            return redirect()->route('EventCategoryList')->with('error', 'you have no permission to delete this category because this category is already assign to Events.');
        }
        if ($EventCategory->delete()) {
            return redirect()->route('EventCategoryList')->with('message', 'Activity Deleted Successfuly ');
        }
    }
}
