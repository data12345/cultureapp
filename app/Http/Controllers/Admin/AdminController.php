<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use Validator;
use Hash;
use Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $validator = Validator::make($request->all(), [
        'name'    => 'required',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = User::find($id);
        $user->name = $request->name;

        if ($user->save()) {
        return redirect()->back()->with('message', 'Profile Detail Update Successfuly ');
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function changePassword(Request $request, $id){
        $validator = Validator::make($request->all(), [
        'old_password'    => 'required',
        'new_password'    => 'required|min:6',
        'confirm_password'    => 'required|min:6|same:new_password',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = User::find(Auth::id());
         if (!(Hash::check($request->get('old_password'), $user->password))) {
            return redirect()->back()->with('error', 'Old password does not match');
        }

        $user->password = Hash::make($request->new_password);
        if ($user->save()) {
        return redirect()->back()->with('message', 'Password Change Successfuly ');
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }
}
