<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Activity;
use Session;
use Validator;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list'] = Activity::Active()->get();
        return view('activity.list', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('activity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
            'end_date' => 'required',
            'time' => 'required',
            'register_link' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $activity = new Activity;
        if ($request->hasFile('image')) {
            //get image file.
            $image = $request->image;
            //get just extension.
            $ext = $image->getClientOriginalExtension();
            //make a unique name
            $filename = uniqid() . '.' . $ext;

            request()->image->move(public_path('activityImages'), $filename);
            $activity->image = $filename;

        }
        $activity->title = $request->title;
        $activity->description = $request->description;
        $activity->end_date = $request->end_date;
        $activity->time = $request->time;
        $activity->register_link = $request->register_link;

        if ($activity->save()) {
            return redirect()->route('ActivityList')->with('message', 'Activity Added Successfuly ');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = Activity::Active()->find($id);
        return view('activity.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'end_date' => 'required',
            'time' => 'required',
            'register_link' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $activity = Activity::find($id);

        if ($request->hasFile('image')) {

            //get image file.
            $image = $request->image;
            //get just extension.
            $ext = $image->getClientOriginalExtension();
            //make a unique name
            $filename = uniqid() . '.' . $ext;

            request()->image->move(public_path('activityImages'), $filename);
            $activity->image = $filename;

        }
        $activity->title = $request->title;
        $activity->description = $request->description;
        $activity->end_date = $request->end_date;
        $activity->time = $request->time;
        $activity->register_link = $request->register_link;

        if ($activity->save()) {
            return redirect()->route('ActivityList')->with('message', 'Activity Update Successfuly ');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::find($id);
        if ($activity->delete()) {
            return redirect()->route('ActivityList')->with('message', 'Activity Deleted Successfuly ');
        }
    }
}
