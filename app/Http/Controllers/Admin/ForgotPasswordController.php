<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\User;
use Hash;
use Session;
use Auth;


class ForgotPasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendForgotPasswordOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $pin = mt_rand(1000, 9999);
        $otp = str_shuffle($pin);
        $userEmailValidation = User::where(['email' => $request->email])->where('user_type', '1')->first();
        if ($userEmailValidation) {
            $user = User::where('email', $request->email)->first();
            $user->otp = $otp;
            if ($user->save()) {
                $link=url('/ForgotPassword/'.$request->email);
                $data = array('email' => $request->email, "otp" => $otp,"link"=>$link);
                Mail::send('email_template.forgotPassword', $data, function ($message) use ($request) {
                    $message->to($request->email, 'New User')
                        ->subject('Otp For Login');
                    $message->from('ashokapptunix30592@gmail.com', 'New User');
                });
                return redirect()->back()->with('message', 'Mail sent. Please check your Email');
            }
        }else {
            return redirect()->back()->with('error', 'Email not exist.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ForgotPassword()
    {
       return  view('auth.passwords.confirm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
            'password' => 'required',
            'confirm_Password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 200);
        }
        $user = User::where('email',$request->email)->where('otp',$request->otp)->first();
        if ($user) {
            $user->password = Hash::make($request->password);
            if ($user->save()) {
                return redirect()->back()->with('message', 'Password Change Successfuly ');
            } else {
                return redirect()->back()->with('error', 'Something went wrong');
            }
        }else{
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
