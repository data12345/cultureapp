<?php

namespace App\Http\Controllers\Admin;

use App\EventCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use Session;
use Validator;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list'] = Event::Active()->with('Category')->get();
        return view('event.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category']=EventCategory::get();
        return view('event.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'image' => 'required',
            'end_date' => 'required',
            'time' => 'required',
            'category' => 'required',
            'register_link' => 'required',
            'website_link' => 'required',
            'free_admission' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $event = new Event;
        if ($request->hasFile('image')) {
            //get image file.
            $image = $request->image;
            //get just extension.
            $ext = $image->getClientOriginalExtension();
            //make a unique name
            $filename = uniqid() . '.' . $ext;

            request()->image->move(public_path('eventImages'), $filename);
            $event->image = $filename;

        }
        $exploded = $this->multiexplode(array("(",","," ",")"),$request->latLong);

        if (count($exploded) > 0){
            $lat=$exploded[1];
            $Long=$exploded[3];
        }

        $event->title = $request->title;
        $event->description = $request->description;
        $event->end_date = $request->end_date;
        $event->category_id = $request->category;
        $event->time = $request->time;
        $event->register_link = $request->register_link;
        $event->website_link = $request->website_link;
        $event->address = $request->address;
        $event->free_admission = $request->free_admission;
        if (isset($lat)){
            $event->latitude =  $lat;
        }
        if (isset($Long)){
            $event->longitude =  $Long;
        }

        if ($event->save()) {
            return redirect()->route('EventList')->with('message', 'Event Added Successfuly ');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }
    }

    public function multiExplode($delimiters,$string) {
        return explode(
            $delimiters[0],
            strtr(
                $string,
                array_combine(
                    array_slice(    $delimiters, 1  ),
                    array_fill(
                        0,
                        count($delimiters)-1,
                        array_shift($delimiters)
                    )
                )
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category']=EventCategory::get();
        $data['data'] = Event::Active()->find($id);
        return view('event.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'end_date' => 'required',
            'time' => 'required',
            'register_link' => 'required',
            'address' => 'required',
            'website_link' => 'required',
            'free_admission' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $event = Event::find($id);

        if ($request->hasFile('image')) {
            //get image file.
            $image = $request->image;
            //get just extension.
            $ext = $image->getClientOriginalExtension();
            //make a unique name
            $filename = uniqid() . '.' . $ext;

            request()->image->move(public_path('eventImages'), $filename);
            $event->image = $filename;

        }
        $exploded = $this->multiexplode(array("(",","," ",")"),$request->latLong);
        if (count($exploded) > 1){
            $lat=$exploded[1];
            $Long=$exploded[3];
        }
        $event->title = $request->title;
        $event->description = $request->description;
        $event->end_date = $request->end_date;
        $event->time = $request->time;
        $event->category_id = $request->category;
        $event->register_link = $request->register_link;
        $event->website_link = $request->website_link;
        $event->free_admission = $request->free_admission;
        $event->address = $request->address;
        if (isset($lat)){
            $event->latitude =  $lat;
        }
        if (isset($Long)){
            $event->longitude =  $Long;
        }

        if ($event->save()) {
            return redirect()->route('EventList')->with('message', 'Event Update Successfuly ');
        } else {
            return redirect()->back()->with('error', 'Something went wrong');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        if ($event->delete()) {
            return redirect()->route('EventList')->with('message', 'Event Deleted Successfuly ');
        }
    }
}
