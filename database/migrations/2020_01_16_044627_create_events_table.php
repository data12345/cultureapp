<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',100);
              $table->string('description',255);
              $table->string('image',100);
              $table->dateTime('end_date')->comment('end date activity');
              $table->string('register_link')->default('');
              $table->string('website_link')->default('');
              $table->string('free_admission')->default('');
              $table->string('address',255)->default('');
              $table->string('latitude',255)->default('');
              $table->string('logitude',255)->default('');
              $table->string('status')->default('1')->comment('1 for activate , 0 for deactivate');
            $table->timestamps(); 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
